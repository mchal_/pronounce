from flask import Flask, request

from pronounce.page import get_page, PageSource, info
from pronounce.page import error as _error


def init():
    info("Pronounce is live!")
    return Flask(__name__, template_folder="static")


app = init()


@app.errorhandler(Exception)
def error(e):
    info(f"Error: {e}", is_error=True)
    return _error(str(e), request.url).render()


@app.route("/robots.txt")
def robots():
    info(f"robots.txt Accessed! UA: {request.user_agent}")
    return "User-Agent: * Disallow: /"


@app.route("/")
def root():
    info("Displaying root page")
    return get_page(PageSource.FILE, "frontpage.yml", request.url).render()


@app.route("/<username>", strict_slashes=False)
def user(username):
    info(f"Displaying user page for {username}")
    return get_page(PageSource.URL, username, request.url).render()


def run():
    app.run(threaded=True)
