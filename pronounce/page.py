from dataclasses import dataclass
from urllib.request import urlopen
from urllib.error import HTTPError
from enum import Enum

import yaml
from flask import render_template

from pronounce.utils import info, getcolor


class PageSource(Enum):
    FILE = (1,)
    URL = (2,)


with open("conf.yml", "r") as global_conf_file:
    global_conf = yaml.load(global_conf_file, Loader=yaml.UnsafeLoader)


def url_iterate(urls: list, username: str):
    for url in urls:
        url = url.replace("{username}", f"{username}", 1)

        try:
            response = urlopen(url.replace("{username}", f"{username}"))
        except HTTPError:
            if username[-1] == "_":
                try:
                    # Try without the final character, as on GitLab if your username ends with a special character
                    # e.g., `mchal_`, you *cannot* create a username/username repository.
                    response = urlopen(url.replace("{username}", f"{username[0: -1]}"))
                except HTTPError:
                    continue
            else:
                continue

        if response.code == 200:
            return response

    return None


@dataclass
class Page:
    title: str
    icon: str
    color: str
    name: str
    age: str

    pronouns: list
    contacts: list
    urls: list

    def render(self):
        return render_template(
            "template.html",
            title=self.title,
            icon=self.icon,
            color=self.color,
            name=self.name,
            age=self.age,
            pronouns=", ".join(self.pronouns),
            contacts=self.contacts,
            urls=(
                lambda x: [f"https://{i}" if not i.startswith("http") else i for i in x]
            )(self.urls),
        )


def error(e, url: str):
    return Page(
        title="Something Went Wrong!",
        icon="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/twitter/53/cross-mark_274c.png",
        color="#aa0000",
        name="Pronounce",
        age="v2.0.0",
        pronouns=[
            f"Something has gone wrong! Error information is available below. If you believe this is an error, please report it to the site owner, or at the project repository on the last URL on this page."
        ],
        contacts=[e],
        urls=[url, "https://gitlab.com/mchal_/pronounce"],
    )


def get_page(source_type: PageSource, source: str, url):
    if source_type == PageSource.URL:
        response = url_iterate(global_conf["urls"], source)

        if response is None:
            info(f"No user found for {source}", is_error=True)
            return error("All URLs returned 404, user not found!", url)

        yml = response.read().decode("utf-8")
    else:
        yml = open(source, "r")

    conf = yaml.load(yml, Loader=yaml.FullLoader)

    return Page(
        title=conf["title"],
        icon=conf["icon"],
        color="#%02x%02x%02x" % getcolor(conf["icon"]),
        name=conf["name"],
        age=conf["age"],
        pronouns=conf["pronouns"],
        contacts=conf["contacts"],
        urls=conf["urls"],
    )
