import pytz

from datetime import datetime

from colorthief import ColorThief
from urllib.request import urlopen
from urllib.error import HTTPError


def info(text: str, is_error: bool = False):
    if is_error:
        symbol = "❌"
    else:
        symbol = "🌈"

    time = datetime.now(pytz.utc).strftime("%Y-%m-%d %H:%M:%S %z")

    print(f"[{time}] {symbol} {text}")


def getcolor(url: str):
    """
    Gets dominant color from an image. Adapted from:
    https://stackoverflow.com/questions/3241929/python-find-dominant-most-common-color-in-an-image
    """
    try:
        data = urlopen(url)
    except HTTPError:
        return 0, 0, 0

    color_thief = ColorThief(data)
    dominant_color = color_thief.get_color(quality=1)

    return dominant_color
