# Pronounce
Minimalist and configurable personal info page

## 💡 Features

- Easy to set up and configure
- Takes advantage of something almost every developer has already, a GitHub profile!

## ❓ How to use

### Hosting

Simply use a service/cronjob to run the following command in the Pronounce directory:

```bash
  $ poetry run gunicorn pronounce.app:app -b 0.0.0.0:${PORT}
```

With ${PORT} being the desired port to run Pronounce on (if behind a proxy)

### Your own Pronounce page!

By default, Pronounce pulls from your username/username repository on either GitLab or GitHub,
e.g., if your GitHub username is "not-my-segfault", you'd visit https://pronounce.domain/not-my-segfault

Pronounce will then look at GitLab first, if it doesn't find not-my-segfault/not-my-segfault there with `pronounce.yml`
at the root, it'll look down the list (by default, on GitHub too)

The format for the `pronounce.yml` file is as follows:
```yaml
---
title: Hi!    # The Page Title
icon: <url>   # The Icon to use for the Page
name: Foo Bar # The main header text for the page
age: 255      # Supertext to show after the header text, it doesn't have to be your age!
pronouns:     # A list of pronoun sets to display
    - "Ex/ample"
    - "Second/Example"
contacts:     # A list of contact information
    - "E-Mail: example@foo.bar"
    - "Mastodon: @example@foo.bar"
urls:         # A list of URLs to display on your page
    - "https://url.com"
    - "https://web.site"
```

## 💾 Installation

**Install Pronounce using `nix profile`**
```bash
  $ nix profile install gitlab:mchal_/pronounce
  $ pronounce
```

## 📸 Screenshots

![Pronounce Screenshot](https://media.discordapp.net/attachments/799373955324313613/1046246179693932595/image.png?width=838&height=642)

## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)
