{
  description = "Minimalist and configurable personal info page";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    utils.url = "github:numtide/flake-utils";
    poetry.url = "github:nix-community/poetry2nix";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    poetry,
    ...
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [poetry.overlay];
      };
      inherit (pkgs) poetry2nix;
      pronounceEnv = poetry2nix.mkPoetryEnv {
        projectDir = ./.;
        preferWheels = true;
      };
    in rec {
      packages.pronounce = poetry2nix.mkPoetryApplication {
        projectDir = ./.;
        python = pkgs.python39;
        preferWheels = true;
      };

      packages.default = packages.pronounce;

      apps.pronounce = utils.lib.mkApp {
        drv = packages.pronounce;
      };

      apps.default = apps.pronounce;

      devShells.default = pronounceEnv.env.overrideAttrs (oldAttrs: {
        buildInputs = [pkgs.python39Packages.black pkgs.python39Packages.poetry];
      });

      formatter = pkgs.alejandra;
    });
}
